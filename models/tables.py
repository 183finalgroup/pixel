#########################################################################
## Define your tables below; for example
##
## >>> db.define_table('mytable',Field('myfield','string'))
##
## Fields can be 'string','text','password','integer','double','boolean'
##       'date','time','datetime','blob','upload', 'reference TABLENAME'
## There is an implicit 'id integer autoincrement' field
## Consult manual for more options, validators, etc.
##
## More API examples for controllers:
##
## >>> db.mytable.insert(myfield='value')
## >>> rows=db(db.mytable.myfield=='value').select(db.mytable.ALL)
## >>> for row in rows: print row.id, row.myfield
#########################################################################
from datetime import datetime

db.define_table('images',
                Field('name', 'string', required=True),
                Field('uploader', db.auth_user),
                Field('upload_time', 'datetime', default=datetime.utcnow()),
                Field('description', 'string', required=True),
                Field('image', 'upload', required=True, requires=IS_NOT_EMPTY()),
                Field('comments', 'list:string'),
                Field('num_ratings', 'integer', default=0),
                Field('sum_ratings', 'integer', default=0),
                Field('rating', 'integer', default=0),
                Field('is_draft', 'boolean', default=False))



db.images.uploader.readable = db.images.uploader.writable = False
db.images.uploader.default = auth.user_id
db.images.name.default = 'Untitled'
db.images.description.default = 'No description'
db.images.num_ratings.readable = False
db.images.num_ratings.writable = False
db.images.sum_ratings.readable = False
db.images.sum_ratings.writable = False
db.images.comments.readable = False
db.images.comments.writable = False
db.images.is_draft.readable = False
db.images.is_draft.writable = False
db.images.upload_time.readable = True
db.images.upload_time.writable = False
db.images.rating.readable = False
db.images.rating.writable = False


db.define_table('opinion',
                Field('info', 'text', required=True),
                Field('opinion_time', 'datetime', default=datetime.utcnow()),
                Field('author', db.auth_user, default=auth.user_id))