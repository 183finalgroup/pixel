# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

import json
import urllib2
import random
import base64
import urllib
import os
from collections import OrderedDict

def index():

    return dict()

def gallery():
    images = db(db.images.uploader == auth.user_id).select()

    def get_num_stars(img_idx):
        if not auth.user_id:
            return None
        r = db((db.images.uploader == auth.user_id) & (db.images.id == img_idx)).select().first()
        return None if r is None else r.rating

    return dict(images=images)


@auth.requires_signature()
def single():
    id = request.args(0)
    image = db.images(id).image
    description = db.images(id).description
    name = db.images(id).name
    return dict(image=image, image_id=id, desc=description, name=name)

def feed():
    return dict()

def scroll():
    images = db(db.images).select(orderby=~db.images.upload_time)
    i = 0
    for image in images:
        if i == 0:
            image['first'] = True
        else:
            image['first'] = False
        i = i + 1
        if image['num_ratings'] != 0:
            db.images.update_or_insert((db.images.id == image['id']),
                rating=image['sum_ratings']/image['num_ratings'])
    return dict(image_dict=images)

@auth.requires_signature()
def overwrite_image():
    new_image = request.vars.image
    image_id = request.args(0)
    image_name = db.images(image_id).name

    current_image = db.images(image_id).image

    if 'base64,' in new_image:
        index = new_image.find('base64,')
        new_image = new_image[index+7:]

    try:
        new_name = image_id + image_name + '.png'
        file = open(new_name, 'wb+')
        decoded_image = base64.urlsafe_b64decode(new_image)
        file.write(decoded_image)
        file.close()

        f = open(new_name, "rb+")
        db.images.update_or_insert((db.images.id == image_id),
        image=f)

    except IOError as e:
        print "I/O error({0}): {1}".format(e.errno, e.strerror)

    return "ok"

@auth.requires_login()
def delete_image():
    ID = request.args(0)
    db(db.images.id == ID).delete()
    redirect(URL('default', 'gallery'))

@auth.requires_signature()
def overwrite_desc():
    id = request.args(0)
    db.images.update_or_insert((db.images.id == id),
            description=request.vars.new_description)
    return "ok"

@auth.requires_signature()
def overwrite_name():
    id = request.args(0)
    db.images.update_or_insert((db.images.id == id),
            name=request.vars.new_name)
    return "ok"

@auth.requires_signature()
def add_comment():
    id = request.vars.id
    image = db.images(id)
    new_comments = image['comments']
    if new_comments is not None:
        new_comments.append(request.vars.new_comment)
    else:
        new_comments = [request.vars.new_comment]
    db.images.update_or_insert((db.images.id == id),
            comments=new_comments)
    return "ok"

@auth.requires_signature()
def vote():
    picid = int(request.vars.picid)
    num_stars = int(request.vars.rating)
    db.images.update_or_insert(
        ((db.images.id == picid) & (db.images.uploader == auth.user_id)),
        rating = num_stars
    )
    return "ok"

def load_images():
    """Loads recent images across all users."""

    images = db(db.images).select(orderby=~db.images.upload_time)
    for image in images:
        if image['num_ratings'] != 0:
            db.images.update_or_insert((db.images.id == image['id']),
                rating=image['sum_ratings']/image['num_ratings'])

    d = {i.id: {      'name'       : i.name,
                      'id'         : i.id,
                      'image'      : i.image,
                      'uploader'   : i.uploader,
                      'upload_time': i.upload_time,
                      'description': i.description,
                      'rating'     : i.rating,
                      'comments'   : i.comments
                }
         for i in images}


    return response.json(dict(image_dict=d))


def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/manage_users (requires membership in
    http://..../[app]/default/user/bulk_register
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """
    return dict(form=auth())

def web2py_bar():
    return dict()

@cache.action()
def download():
    return response.download(request, db)

@auth.requires_login()
def upload():
    gallery_url = "../default/gallery"
    record = db.images(request.args(0))
    form = SQLFORM(db.images, record, deletable=True,
                   upload = URL('downlad'))
    if form.process().accepted:
       response.flash = 'form accepted'
       redirect(location=gallery_url)
    elif form.errors:
        response.flash = 'form has errors'
    return dict(form=form)